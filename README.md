<h1>Welcome to ETHIO-BANK</h1>

<h2>This CLI app is built using</h2>
<ul>
<li>Java maven v1.8</li>
<li>AWS - cloud services platform, offering database storage</li>
<li>DBeaver - a SQL client software application and a database administration tool</li>
<li>JDBC (Java Database Connectivity) - the Java API that manages connection to a database </li>
</ul>

<h2>Maven dependencies used</h2>
<ul>
<li>log4j - for logging </li>
<li>ojdbc and</li>
<li>junit - for unit testing</li>
</ul>

<h2>Functionalities</h2>

<ul>
<li>A user can login.</li>
<li>A customer can apply for a new bank account with a starting balance (default - $50.00).</li>
<li>A customer can view the balance in the account.</li>
<li>A customer can make a withdrawal or deposit to a specific account.</li>
<li>As the system, invalid transactions are rejected. Examples:
<ul>
<li>Account with PENDING status</li>
<li>Invalid transaction amount entry (alphabetics -- abc)</li>
<li>A deposit or withdrawal of negative money.</li>
<li>A withdrawal that would result in a negative balance. Informs the customer that the balance isn't suffifcient to do the transaction</li>
</ul>

</li>
<li>An employee can view a customer's bank accounts.</li>
<li>An employee can view a log of all transactions.</li>
</ul>


