package dev.sol.daos;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dev.sol.models.User;

import java.time.LocalDate;

public class TransactionDaoImplTest {



	private TransactionDaoImpl transactionDaoImpl = new TransactionDaoImpl();
	static LocalDate today = LocalDate.now();

	User user = new User();


	

	@Test
	public void testWithNegativeDepositAmount() {
		double expected = user.getCurrentBalance();
		double actual = transactionDaoImpl.makeDeposit(user, -10, today);
		assertEquals(expected, actual,0.01);

	}

	@Test
	public void testWithValidDepositAmount() {
		double expected = user.getCurrentBalance() + 100;
		double actual = transactionDaoImpl.makeDeposit(user, 100, today);
		assertEquals(expected, actual,0.01);

	}


}
