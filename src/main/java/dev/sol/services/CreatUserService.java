package dev.sol.services;


import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

import dev.sol.daos.AccountDaoImpl;
import dev.sol.daos.TransactionDaoImpl;
import dev.sol.daos.UserDaoImpl;
import dev.sol.models.*;


public class CreatUserService {

	static Logger log = Logger.getRootLogger();

	static LocalDate today = LocalDate.now();
	Scanner scanner = new Scanner(System.in);
	Scanner myObj = new Scanner(System.in);  // Create a Scanner object

	UserDaoImpl udi = new UserDaoImpl();
	AccountDaoImpl adi = new AccountDaoImpl();

	TransactionDaoImpl tdi = new TransactionDaoImpl();


	private List<User> allUsers = udi.getAllUser();
	private List<Transaction> allTransactions = tdi.getAllTransactions();

	User user = new User();



	public User createUser() {
		return null;

	}

	public void welcome() {
		System.out.println("Welcome to ETHIO-BANK");
		promptUserToLogIn();
	}


	// prompt user for login or signup
	public Object promptUserToLogIn() {
		String selection = null;

		System.out.println("(1) LogIn (2) New to our bank? ");

		selection = scanner.next();

		switch (selection) {
		case "1":
			return logInUser();
		case "2":
			return createNewUser(user);

		default:
			System.out.println("Invalid entry, please try again!");
			return promptUserToLogIn();
		}

	}


	// login user
	public User logInUser() {

		String userName;
		String password;

		System.out.println("Enter username: ");
		userName = myObj.nextLine();

		System.out.println("Enter your Password: ");
		password = myObj.nextLine();


		for(User aUser : allUsers) {

			try {

				if(aUser.getUserName().equals(userName) && aUser.getPassword().equals(password)) {
					user = aUser;
					break;
				}

				//				else {
				//					System.out.println("Invalid username or password, please try again!");
				//					return logInUser();
				//				}

			} catch (Exception e) {
				log.error("Exeception was thrown " + e.getClass());
			}

		}

		System.out.println("\nWelcome " + user.getFirstName() + " " + user.getLastName());
		userServices();

		return user;

	}



	public void userServices() {

		String customer = Role.CUSTOMER.name();

		if(user.getRole().toString() == customer) {
			customerServices();
		}

		else {
			System.out.println("");
			employeeServices();}
	}




	public void customerServices() {
		System.out.println("\nWhat do you need to do today? \n1/ Current balance ");
		System.out.println("2/ Deposit");
		System.out.println("3/ Withdraw");
		System.out.println("4/ My profile");
		System.out.println("5/ My Bank account details");
		System.out.println("6/ Exit");
		System.out.println("");

		String selection = scanner.next();

		switch (selection) {
		case "1":
			System.out.println("");
			System.out.println("Your current balance as of " + today + " is $" + user.getCurrentBalance());
			System.out.println("");
			userServices();
			break;

		case "2":
			System.out.println("");
			tdi.makeDeposit(user, 0.0, today);
			System.out.println("");
			userServices();
			break;
		case "3":
			System.out.println("");
			tdi.makeWithdraw(user, 0.0, today);
			System.out.println("");
			userServices();
			break;
		case "4":
			System.out.println("");
			System.out.println(user.userDetail());
			System.out.println("");
			userServices();
			break;
		case "5":
			System.out.println("");
			System.out.println(user.bankDetail());
			System.out.println("");
			userServices();
			break;
		case "6":
			System.out.println("");
			welcome();
			System.out.println("");
			break;

		default:
			System.out.println("");
			System.out.println("Invalid entry, please try again!");
			userServices();
		}

	}



	public void employeeServices() {


		System.out.println("\n1/ Check all transactions ");
		System.out.println("2/ Pending accounts");
		System.out.println("3/ --------- ");
		System.out.println("4/ --------- ");
		System.out.println("5/ ---------");
		System.out.println("6/ Exit");
		System.out.println("");

		String selection = scanner.next();

		switch (selection) {
		case "1":
			System.out.println(" ");
			System.out.println("All transactions as of " + today);
			System.out.println("Count: " + allTransactions.size());
			for(int i=0; i < allTransactions.size(); i++ ) {
				System.out.println("");
				System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
				System.out.println("");
				System.out.print(i+1 + "/ ");
				System.out.println(allTransactions.get(i));
				System.out.println("");
				System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");

			}
			employeeServices();
			break;

		case "2":
			System.out.println("");
			adi.getAccountInfo();
			employeeServices();
			break;
		case "3":
			System.out.println("");
			employeeServices();
			break;
		case "4":
			System.out.println("");
			employeeServices();
			break;
		case "5":
			System.out.println("");
			employeeServices();
			break;
		case "6":
			System.out.println("");
			welcome();
			System.out.println("");
			break;

		default:
			System.out.println("");
			System.out.println("Invalid entry, please try again!");
			userServices();
		}



	}


	// fill form to create new account
	public User createNewUser(User user) {
		return udi.addNewUser(user);
	}


}
