package dev.sol.models;

import java.time.LocalDate;
import java.util.ArrayList;

public class User {
	
	static LocalDate today = LocalDate.now();
	
	private String firstName;
	private String lastName;
	private String userName;
	private String password;
	private String email;
	private Role role;
	private int accountNumber;
	private double initialBalance;
	private double currentBalance;
	private AccountStatus status;
	
	
	
	public User() {
		super();
	}


	
	public User(String firstName, String lastName, String userName, String password, String email, Role role) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.role = role;
	}

	public User(String firstName, String lastName, String userName, String password, String email, Role role,
			int accountNumber, double initialBalance, double currentBalance, AccountStatus status) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.role = role;
		this.accountNumber = accountNumber;
		this.initialBalance = initialBalance;
		this.currentBalance = currentBalance;
		this.status = status;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public Role getRole() {
		return role;
	}



	public void setRole(Role role) {
		this.role = role;
	}



	public int getAccountNumber() {
		return accountNumber;
	}



	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}



	public double getInitialBalance() {
		return initialBalance;
	}



	public void setInitialBalance(double initialBalance) {
		this.initialBalance = initialBalance;
	}



	public double getCurrentBalance() {
		return currentBalance;
	}



	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}



	public AccountStatus getStatus() {
		return status;
	}



	public void setStatus(AccountStatus status) {
		this.status = status;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + accountNumber;
		long temp;
		temp = Double.doubleToLongBits(currentBalance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		temp = Double.doubleToLongBits(initialBalance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (accountNumber != other.accountNumber)
			return false;
		if (Double.doubleToLongBits(currentBalance) != Double.doubleToLongBits(other.currentBalance))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (Double.doubleToLongBits(initialBalance) != Double.doubleToLongBits(other.initialBalance))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (role != other.role)
			return false;
		if (status != other.status)
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "FirstName: " + firstName + ", LastName: " + lastName;
	}

	
	public String userDetail() {
		return "\nWelcome " + firstName + " " + lastName + "\n \nYour userName: " + userName + "\nPassword: "
				+ password + "\nE-mail: " + email;
		
	}
	
	public String bankDetail() {
		return  "\nAccount Detail" + "\nAccountNumber: " + accountNumber
				+ "\nCurrentBalance: $" + currentBalance + "\nAccount status: " + status;
		
	}
	

	public Object userCurrentBalance() {
		return  "Your current balance as of " + today + " is $" + currentBalance;
	}
	
	
}
