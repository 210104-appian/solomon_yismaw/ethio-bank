package dev.sol.models;

public enum AccountStatus {
	PENDING, ACTIVE, SUSPENDED, CLOSED
}
