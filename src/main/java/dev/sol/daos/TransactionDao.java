package dev.sol.daos;

import java.time.LocalDate;
import java.util.List;

import dev.sol.models.Transaction;
import dev.sol.models.User;


public interface TransactionDao {
	
	public List<Transaction> getAllTransactions();
	
	public double makeDeposit(User user, double amount, LocalDate date);
	
	public double makeWithdraw(User user, double amount, LocalDate date);
	
	

}
