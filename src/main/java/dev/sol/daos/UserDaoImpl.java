package dev.sol.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

import dev.sol.models.Account;
import dev.sol.models.AccountStatus;
import dev.sol.models.Role;
import dev.sol.models.User;
import dev.sol.util.ConnectionUtil;

public class UserDaoImpl implements UserDao {


	static Account account = new Account();
	static Logger log = Logger.getRootLogger();
	Scanner scanner = new Scanner(System.in);
	List <User> allUsers = new ArrayList<>();

	@Override
	public List<User> getAllUser() {


		try(Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement();) {
			ResultSet resultSet = statement.executeQuery("SELECT * FROM ALL_CUSTOMERS_WITH_ACCOUNT");

			while(resultSet.next()) {

				User user = new User();
				user.setFirstName(resultSet.getString("FIRST_NAME"));
				user.setLastName(resultSet.getString("LAST_NAME"));
				user.setUserName(resultSet.getString("USER_NAME"));
				user.setPassword(resultSet.getString("PASSWORD"));
				user.setEmail(resultSet.getString("EMAIL"));
				user.setAccountNumber(resultSet.getInt("ACCOUNT_NUMBER"));
				user.setCurrentBalance(resultSet.getInt("CURRENT_BALANCE"));

				String employeeRole = resultSet.getString("EMPLOYMEE_ROLE");
				if(employeeRole != null) {
					Role role = Role.valueOf(employeeRole);
					user.setRole(role);
				}

				String accountStatus = resultSet.getString("ACCOUNT_STATUS");
				if(accountStatus != null) {
					AccountStatus status = AccountStatus.valueOf(accountStatus);
					user.setStatus(status);
				}

				allUsers.add(user);
			}

		} catch (Exception e) {
			//			e.printStackTrace();
			log.error("Execption was thrown " + e.getClass());
		}
		return allUsers;
	}



	@Override
	public User addNewUser(User user) {

		String addNewUserSQL = "INSERT INTO CUSTOMER (FIRST_NAME, LAST_NAME, EMAIL, USER_NAME, PASSWORD, EMPLOYMEE_ROLE) VALUES (?,?,?,?,?,?)";
		String addNewAccountSQL = "INSERT INTO ACCOUNT (CUSTOMER_ID, INITIAL_BALANCE, CURRENT_BALANCE, ACCOUNT_STATUS) VALUES (?,?,?,?)";
		String idSQL = "SELECT MAX(CUSTOMER_ID) FROM CUSTOMER";

		String firstName;
		String lastName;
		String email;
		String userName;
		String password;

		System.out.println("Enter your First name: ");
		firstName = scanner.next().toUpperCase();

		System.out.println("Enter your Last name: ");
		lastName = scanner.next().toUpperCase();

		System.out.println("Enter user email address: ");
		email = scanner.next();

		System.out.println("Enter user name and please keep note on your username for future login: ");
		userName = scanner.next();

		System.out.println("Enter password for your new account and please keep note on your password for future login: ");
		password = scanner.next();


		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setUserName(userName);
		user.setPassword(password);

		if (allUsers.contains(user)) {
			System.out.println("You have account, please login!");
			//			return user;
		}
		else {


			try (Connection connection = ConnectionUtil.getConnection();

					PreparedStatement addNewUserStatement = connection.prepareStatement(addNewUserSQL);
					PreparedStatement addNewAccountStatement = connection.prepareStatement(addNewAccountSQL);
					Statement statement = connection.createStatement();) {

				addNewUserStatement.setString(1, firstName);
				addNewUserStatement.setString(2, lastName);
				addNewUserStatement.setString(3, email);
				addNewUserStatement.setString(4, userName);
				addNewUserStatement.setString(5, password);
				addNewUserStatement.setString(6, "CUSTOMER");
				addNewUserStatement.execute();



				ResultSet rs = statement.executeQuery(idSQL);

				if(rs.next()) {

					int customerId = rs.getInt(1);	
					addNewAccountStatement.setInt(1, customerId);
					addNewAccountStatement.setDouble(2, 50);
					addNewAccountStatement.setDouble(3, 50);
					addNewAccountStatement.setString(4, "PENDING");
					addNewAccountStatement.execute();
				}

				System.out.println("Your account has been initialized, please note your credentials " + user.userDetail() + user.bankDetail());
				return user;

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

		return user;

	}

	@Override
	public boolean ChangeUserInfo(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeUserByID(User user) {
		// TODO Auto-generated method stub
		return false;
	}


}
