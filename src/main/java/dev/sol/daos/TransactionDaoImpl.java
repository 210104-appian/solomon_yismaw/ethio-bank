package dev.sol.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

import dev.sol.models.Account;
import dev.sol.models.Transaction;
import dev.sol.models.User;
import dev.sol.util.ConnectionUtil;

public class TransactionDaoImpl implements TransactionDao{

	ArrayList <Transaction> allTransactions = new ArrayList <Transaction>();
	private Scanner scanner = new Scanner(System.in);

	static Logger log = Logger.getRootLogger();


	public TransactionDaoImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Transaction> getAllTransactions() {
		log.info("Getting all transactions!");
		Account account = new Account();
		User user = new User();

		try (Connection conncetion = ConnectionUtil.getConnection();
				Statement statement = conncetion.createStatement();) {
			ResultSet resultSet = statement.executeQuery("SELECT * FROM TRANSACTION_WITH_ACCOUNT_DETAIL");

			while(resultSet.next()) {

				Transaction newTransaction = new Transaction();

				account.setAccountNumber(resultSet.getInt("ACCOUNT_NUMBER"));
				account.setCurrentBalance(resultSet.getDouble("CURRENT_BALANCE"));

				user.setFirstName(resultSet.getString("FIRST_NAME"));
				user.setLastName(resultSet.getString("LAST_NAME"));

				account.setAccountHolder(user);

				newTransaction.setAccount(account);
				newTransaction.setAmount(resultSet.getDouble("AMOUNT"));
				newTransaction.setType(resultSet.getString("TRANSACTION_TYPE"));

				allTransactions.add(newTransaction);
			}
			
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("Exception was thrown " + e.getClass());
		}

		return allTransactions;
	}



	public double makeDeposit(User user, double amount, LocalDate date) {
		log.info("User is making a deposit!");

		String transactionSQL = "INSERT INTO MONEY_TRANSACTION (ACCOUNT_NUMBER, AMOUNT, TRANSACTION_TYPE) VALUES (?, ?,?)";
		String updateCurrentBalance = "UPDATE ACCOUNT SET CURRENT_BALANCE = ? WHERE ACCOUNT_NUMBER = ?";


		System.out.println("Enter the amount");
		String selectionAmount = scanner.next();

		try (Connection connection = ConnectionUtil.getConnection();

				PreparedStatement transactionStatement = connection.prepareStatement(transactionSQL);
				PreparedStatement updateCurrentBalanceSt = connection.prepareStatement(updateCurrentBalance);
				) {

			if(user.getStatus().name() == "ACTIVE") {
				if(selectionAmount.matches("^\\d+$")) { // check to see if it matches a number
					amount = Double.parseDouble(selectionAmount);
					System.out.println(amount);
					if(amount > 0) {
						System.out.println("");

						transactionStatement.setInt(1, user.getAccountNumber());
						transactionStatement.setDouble(2, amount);
						transactionStatement.setString(3, "DEPOSIT");
						transactionStatement.execute();


						updateCurrentBalanceSt.setDouble(1, user.getCurrentBalance() + amount);
						updateCurrentBalanceSt.setInt(2, user.getAccountNumber());

						// call executeUpdate to execute our sql update statement
						updateCurrentBalanceSt.executeUpdate();
						updateCurrentBalanceSt.close();

						user.setCurrentBalance(user.getCurrentBalance() + amount);
						System.out.println("Successful, your balance is now $" + user.getCurrentBalance());
					}
				}

				else {
					System.out.println("Not a valid entry, Please try again!");
					makeDeposit(user, amount, date);
				}
			}
			else {
				System.out.println("Sorry, your account is not active please reach to your bank!");
			}

		} catch (Exception e) {
			//			e.printStackTrace();
			log.error("Exception was thrown " + e.getClass());
		}
		return user.getCurrentBalance();	
	}



	public double makeWithdraw(User user, double amount, LocalDate date) {

		log.info("User is withdrawing from account!");

		String withdrawalSQL = "INSERT INTO MONEY_TRANSACTION (ACCOUNT_NUMBER, AMOUNT, TRANSACTION_TYPE) VALUES (?, ?,?)";
		String updateCurrentBalance = "UPDATE ACCOUNT SET CURRENT_BALANCE = ? WHERE ACCOUNT_NUMBER = ?";


		System.out.println("Enter the amount you want to withdraw: $ ");
		String selectionAmount = scanner.next();


		try (Connection connection = ConnectionUtil.getConnection();

				PreparedStatement transactionStatement = connection.prepareStatement(withdrawalSQL);
				PreparedStatement updateCurrentBalanceSt = connection.prepareStatement(updateCurrentBalance);
				) {


			if(user.getStatus().name() == "ACTIVE") {
				if(selectionAmount.matches("^\\d+$")) {
					amount = Integer.parseInt(selectionAmount);
					if(amount > 0 && user.getCurrentBalance() > amount) {
						System.out.println("");

						transactionStatement.setInt(1, user.getAccountNumber());
						transactionStatement.setDouble(2, amount);
						transactionStatement.setString(3, "WITHDRAW");
						transactionStatement.execute();

						updateCurrentBalanceSt.setDouble(1, user.getCurrentBalance() - amount);
						updateCurrentBalanceSt.setInt(2, user.getAccountNumber());

						// call executeUpdate to execute our sql update statement
						updateCurrentBalanceSt.executeUpdate();
						updateCurrentBalanceSt.close();

						user.setCurrentBalance(user.getCurrentBalance() - amount);
						System.out.println("Successful, your balance is now $" + user.getCurrentBalance());
					}

					else if(amount > user.getCurrentBalance()) {
						System.out.println("");
						System.out.println("You don't have sufficient amount, your current balance is $" + user.getCurrentBalance() + " please enter less amount!");
						System.out.println("");
						makeWithdraw(user, amount, date);
					}
				}

				else {
					System.out.println("");
					System.out.println("Not a valid entry, Please try again!");
					System.out.println("");
					makeWithdraw(user, amount, date);
				}
			}
			else {
				System.out.println("Sorry, your account is not active please reach to your bank!");
			}

		} catch (Exception e) {
//			e.printStackTrace();
			log.error("Exception was thrown " + e.getClass());
		}

		return user.getCurrentBalance();	
	}
}
