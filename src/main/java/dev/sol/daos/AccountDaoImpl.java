package dev.sol.daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dev.sol.models.Account;
import dev.sol.models.AccountStatus;
import dev.sol.models.Role;
import dev.sol.models.User;
import dev.sol.util.ConnectionUtil;

public class AccountDaoImpl implements AccountDao {

	UserDaoImpl udi = new UserDaoImpl();

	static Logger log = Logger.getRootLogger();

	public AccountDaoImpl() {
		// TODO Auto-generated constructor stub
	}

	List<Account> allAccounts = new ArrayList<>();
	
	@Override
	public List<Account> getAllAccount() {
		

		String sql = "SELECT * FROM TRANSACTION_WITH_ACCOUNT_DETAIL";


		try(Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement();) {

			ResultSet resultSet = statement.executeQuery(sql);

			while(resultSet.next()) {
				Account account = new Account();

				account.setAccountNumber(resultSet.getInt("ACCOUNT_NUMBER"));

				account.setCurrentBalance(resultSet.getInt("CURRENT_BALANCE"));

				String accountStatus = resultSet.getString("ACCOUNT_STATUS");

				if(accountStatus != null) {
					AccountStatus status = AccountStatus.valueOf(accountStatus);
					account.setStatus(status);
				}

				allAccounts.add(account);
			}

		} catch (Exception e) {
//			e.printStackTrace();
			log.error("Exeception was thrown " + e.getClass());
		}
		return allAccounts;
	}

	@Override
	public Account createNewAccount(Account account) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Account getAccountInfo() {
		
//		for (int i = 0; i < allAccounts.size();) {
//			 i++;
//			System.out.println(allAccounts.get(i));
//			return allAccounts.get(i);
//		}

		for(Account account : getAllAccount()) {
			if(account.getStatus().name() == "ACTIVE") {
				System.out.println(account);
//				return account;
			}
			
		}
		return null;
		
	}

	@Override
	public boolean approveUserAccount(Account account) {
		// TODO Auto-generated method stub
		return false;
	}

}
