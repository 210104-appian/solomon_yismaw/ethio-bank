package dev.sol.daos;


import java.util.List;

import dev.sol.models.Account;

public interface AccountDao {

	public List<Account> getAllAccount();
	
	public Account getAccountInfo();
	
	public Account createNewAccount(Account account);
	
	public boolean approveUserAccount(Account account);
	
}
